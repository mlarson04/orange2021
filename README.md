# README #

This repository is for TPR Orange to house and track code for the 2019-2020 season. Using the repository-hosting service called BitBucket, we are able to create varying issues, branches, and other useful solutions to help keep us organized.

We are using a control system based out of 7 main files; 3 for variables declarations and inclusion of other files, 2 for our graphics system, 1 for our autonomous functions, and 1 main file where we are able to call all of our previously defined functions.

To identify areas of our control system and call them when needed, we are using callback functions in our "int main()" to go back and find our autonomous and driver control functions.

In total we are using _ motors and _ sensors, though we would like to add the Vision Sensor to our robot.

As far as mechanisms go, we have a _ , a _ , and a _ . The controls for all of those are fairly basic and located within our "main.cpp" file.
