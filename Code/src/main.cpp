#include "variables.h"

using namespace vex;

void initialize() { // reset/calibrate motors and sensors
  Inertial1.calibrate();
  Vision17.init(1);

  lft.resetRotation();
  rgt.resetRotation();
  roller.resetRotation();
  intake.resetRotation();
}

int main() {
  lcdDraw();
  Brain.Screen.render();

  while (true) {
    lcdTouch();

    while (Competition.isFieldControl() || Competition.isCompetitionSwitch()) {
      while (Competition.isEnabled()) {
        if (Competition.isAutonomous()) {
          Autonomous();
        } else if (Competition.isDriverControl()) {
          driverControl();
        } else {
          initialize();
        }
      }
      lcdTouch();
    }
  }
}