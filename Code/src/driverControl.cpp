#include "variables.h"
#include "PID.h"

void driverControl() { // For manual control portion of match
  while (true) {
    drv.setStopping(brake);
    drivePID.setTarget(joy_LX);
    turnPID.setTarget(joy_LY);
    
    int leftMotorPower = ( drivePID.update(driveL) + turnPID.update(gyro_value) ) * 2;
    int rightMotorPower = ( turnPID.update(gyro_value) - drivePID.update(driveR) ) * 2;
    lft.spin(directionType::undefined, leftMotorPower, rpm); // drivetrain controls
    rgt.spin(directionType::undefined, rightMotorPower, rpm);

    if (master_l1 || partner_l1) { // intake controls
      intake.spin(reverse, 200, rpm);
    } else if (master_l2 || partner_l2) {
      intake.spin(forward, 200, rpm);
    } else {
      intake.stop(brake);
    }

    if (master_r1 || partner_r1) { // roller lift controls
      roller.spin(forward, 600, rpm);
    } else if (master_r2 || partner_r2) {
      roller.spin(reverse, 600, rpm);
    } else {
      roller.stop(brake);
    }
  }
}
