#include "variables.h"

using namespace vex;

using signature = vision::signature;

brain Brain;
competition Competition;

controller Master  = controller(primary);
controller Partner = controller(partner);

motor FrontLeft = motor(PORT5, ratio18_1, true);    // Speed motor; reversed; left drive
motor BackLeft  = motor(PORT4, ratio18_1, true);    // Speed motor; reversed; left drive
motor_group lft = motor_group(FrontLeft, BackLeft);

motor FrontRight = motor(PORT8, ratio18_1, false);   // Speed motor; not reversed; right drive
motor BackRight  = motor(PORT9, ratio18_1, false);   // Speed motor; notreversed; right drive
motor_group rgt  = motor_group(FrontRight, BackRight);

motor LeftIntake = motor(PORT1, ratio18_1, true);   // Speed motor; reversed; roller intake
motor RightIntake = motor(PORT10, ratio18_1, false);  // Speed motor; not reversed; roller intake
motor_group intake = motor_group(LeftIntake, RightIntake);

motor TopRoller = motor(PORT3, ratio6_1, false);     // Turbo motor; not reversed; roller lift
motor BottomRoller = motor(PORT2, ratio6_1, true);  // Turbo motor; nto reversed; roller lift
motor_group roller = motor_group(TopRoller, BottomRoller);

inertial Inertial1 = inertial(PORT11);

//                         left right  gyro_val  wheel-trav  wid  base
smartdrive drv = smartdrive(lft, rgt, Inertial1,   8.63,      18,  12, distanceUnits::in);

signature BLUE_BALL (  1, -2769,      -585,    -1677,  2073,  8973,  5523, 1.300,     0);
signature RED_BALL (   2,  8483,      9169,     8826,  -687,     1,  -343, 3.000,     0);
signature SIG_4 (      4,     0,         0,        0,     0,     0,     0, 3.000,     0);
signature SIG_3 (      3,     0,         0,        0,     0,     0,     0, 3.000,     0);
signature SIG_5 (      5,     0,         0,        0,     0,     0,     0, 3.000,     0);
signature SIG_6 (      6,     0,         0,        0,     0,     0,     0, 3.000,     0);
signature SIG_7 (      7,     0,         0,        0,     0,     0,     0, 3.000,     0);
vision Vision17 ( PORT20,    50, BLUE_BALL, RED_BALL, SIG_3, SIG_4, SIG_5, SIG_6, SIG_7);

int driveFLCurrent = FrontLeft.position(deg);
int driveFRCurrent = FrontRight.position(deg);
int driveBLCurrent = BackLeft.position(deg);
int driveBRCurrent = BackRight.position(deg);

int driveFLRevol = FrontLeft.position(rev);
int driveFRRevol = FrontRight.position(rev);
int driveBLRevol = BackLeft.position(rev);
int driveBRRevol = BackRight.position(rev);

int driveL = (driveFLCurrent + driveBLCurrent) /2;
int driveR = (driveFRCurrent + driveBRCurrent) /2;

int lRevol = (driveFLRevol + driveBLRevol) /2;
int rRevol = (driveFRRevol + driveBRRevol) /2;

bool screenPressed = false;
bool BrainPress = Brain.Screen.pressing();
bool autoDone;
int xPos = Brain.Screen.xPosition();
int yPos = Brain.Screen.yPosition();

int auto_count  = 0, auto_select = 0;
int currentPage = 0, currentMenu = 0;

double accel_value = Inertial1.acceleration(axisType::yaxis);
double gyro_value = Inertial1.heading();