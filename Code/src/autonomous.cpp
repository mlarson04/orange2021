#include "autonomous.h"

void Autonomous() { // Code for the autonomous section of match
  while (true) {
    if (auto_select == 2) {         //REd
      switch (auto_count) {
        //                   lft   rgt  dist. ang   itk  roll   dur. sens.
        case 0 : auto_step(   0,    0,    0,   0,    0,   100,  500, TIME); // intake
        case 1 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 2 : auto_step(  75,   75,    0,   0,    0,     0,  580, TIME); // forward
        case 3 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 4 : auto_step( -55,   55,    0,   0,    0,     0,  475, TIME); // turn
        case 5 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 6 : auto_step(  13,   13,    0,   0,  100,   100, 1475, TIME); // move toward goal
        case 7 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 8 : auto_step(   0,    0,    0,   0,  100,   100, 1500, TIME); // pick up the ball
        case 9 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 10: auto_step(   0,    0,    0,   0,    0,   100,  500, TIME); // shoot ball
        case 12: auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 13: auto_step( -50,  -50,    0,   0, -100,   100,  300, TIME); //
        case 14: auto_step(   0,    0,    0,   0,    0,     0,    0, END ); //
        default: return; 
      }
    } else if (auto_select == 3) {  //ReD 2
      switch (auto_count) {
        //                   lft   rgt  dist. ang   itk  roll   dur. sens.
        case 0 : auto_step(   0,    0,    0,   0,    0,   100,  500, TIME); // intake
        case 1 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 2 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME);
        case 3 : auto_step(   0,    0,    0,   0,    0,     0,    0, END );
        default: return;
      }
    } else if (auto_select == 5) {  //BLUe
      switch (auto_count) {
       //                   lft   rgt  dist. ang   itk  roll   dur. sens.
        case 0 : auto_step(   0,    0,    0,   0,    0,   100,  500, TIME); // intake
        case 1 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 2 : auto_step(  75,   75,    0,   0,    0,     0,  580, TIME); // forward
        case 3 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 4 : auto_step( -55,   55,    0,   0,    0,     0,  475, TIME); // turn
        case 5 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 6 : auto_step(  13,   13,    0,   0,  100,   100, 1475, TIME); // move toward goal
        case 7 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 8 : auto_step(   0,    0,    0,   0,  100,   100, 1500, TIME); // pick up the ball
        case 9 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 10: auto_step(   0,    0,    0,   0,    0,   100,  500, TIME); // shoot ball
        case 12: auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 13: auto_step( -50,  -50,    0,   0, -100,   100,  300, TIME); //
        case 14: auto_step(   0,    0,    0,   0,    0,     0,    0, END ); //
        default: return; 
      }
    } else if (auto_select == 6) {  //BlUe 2: electric boogaloo
      switch (auto_count) {
        //                   lft   rgt  dist. ang   itk  roll   dur. sens.
        case 0 : auto_step(   0,    0,    0,   0,    0,   100,  500, TIME); // intake
        case 1 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME); // |pause|
        case 2 : auto_step(   0,    0,    0,   0,    0,     0,  250, TIME);
        case 3 : auto_step(   0,    0,    0,   0,    0,     0,    0, END );
        default: return;
      }
    } else if (auto_select == 8) {  //SKILLs
      switch (auto_count) {
        //                  lft   rgt   dist. ang  itk  roll   dur.  sens.
        case 0 : auto_step(   0,    0,   0,    0,    0,  600,   200,  TIME); // deploy intake 
        case 1 : auto_step(   0,    0,   0,    0,    0,    0,   250,  TIME); // |pause|
        case 2 : auto_step( 150,  150,   0,    0,    0,    0,  1480,  TIME); // forward
        case 3 : auto_step(-102,  102,   0,    0,    0,    0,  1252,  TIME); // turn toward goal
        case 4 : auto_step(   0,    0,   0,    0,    0,    0,   250,  TIME); // |pause|
        case 5 : auto_step( 200,  200,   0,    0,    0,    0,   525,  TIME); // move towards goal
        case 6 : auto_step(   0,    0,   0,    0,    0,    0,   250,  TIME); // |pause|
        case 7 : auto_step( 125,  125,   0,    0, -200,    0,   350,  TIME); // continue moving, slow down
        case 8 : auto_step( 200,  200,   0,    0, -200,  570,  1300,  TIME); // shoot ball, intake ball
        case 9 : auto_step(   0,    0,   0,    0,    0,    0,   250,  TIME); // |pause|
        case 10: auto_step(-200, -200,   0,    0, -200,    0,  2050,  TIME); // back up, continue intake
        case 11: auto_step(-102,  102,   0,    0,    0,    0,   647,  TIME); // turn toward other goal
        case 12: auto_step(   0,    0,   0,    0,    0,    0,   250,  TIME); // |pause|
        case 13: auto_step( 150,  150,   0,    0,    0,    0,  2000,  TIME); // move forward
        case 14: auto_step(   0,    0,   0,    0,    0,  600,  1500,  TIME); // shoot ball
        case 15: auto_step(-100, -100,   0,    0,    0,    0,   500,  TIME); // back up
        case 16: auto_step(   0,    0,   0,    0,    0,    0,     0,  END ); // end the program
        default: return;
      }
    } else if (auto_select == 10) { //TEsting
      switch (auto_count) {
        //                  lft   rgt  dist  ang  itk  roll  dur   sens
        case 0 : auto_step( 200,  200,   6,   0,   0,   0,    0,   ENCO); // move forward 60 inches
        case 1 : auto_step( 150,  150,   0,  45,   0,   0,    0,   GYRO); // turn 45 degrees
        case 2 : auto_step(   0,    0,   0,   0,   0,   0,  250,   TIME); // |pause|
        case 3 : auto_step( 200,  200,  67,   0,   0,   0,    0,   ENCO); // move towards goal
        case 4 : auto_step(   0,    0,   0,   0,-200, 570, 1300,   TIME); // shoot ball, intake
        case 5 : auto_step(-200, -200,  67,   0,   0,   0,    0,   ENCO); // back up
        case 6 : auto_step( 150,  150,   0,  65,   0,   0,    0,   GYRO); // turn 65 degrees
        case 7 : auto_step( 200,  200,  67,   0,   0,   0,    0,   ENCO); // move towards other goal
        case 8 : auto_step(   0,    0,   0,   0,   0, 600, 1500,   TIME); // shoot ball
        case 9 : auto_step(-100, -100,   5,   0,   0,   0,    0,   ENCO); // backup
        case 10: auto_step(   0,    0,   0,   0,   0,   0,    0,   END ); // end routine
        default: return;
      }
    } else if (auto_select == 11) { //TEsting #2
      switch (auto_count) {
        //                  lft   rgt  dist  ang  itk  roll  dur   sens
        case 0 : auto_step(  0,    0,   0,    0,   0,   0,  250,   TIME);
        case 1 : auto_step(  0,    0,   0,    0,   0,   0,    0,   END );
        default: return;
      }
    } else { //Nothing
      switch (auto_count) {
        //                  lft   rgt  dist  ang  itk  roll  dur   sens
        case 0 : auto_step(  0,    0,   0,    0,   0,   0,  250,   TIME);
        case 1 : auto_step(  0,    0,   0,    0,   0,   0,    0,   END );
        default: return;
      }
    }
  }
}
