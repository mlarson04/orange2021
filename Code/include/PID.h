class PID {
	// Parameters are not visible
	private:

	// Information for describing the PID
	double kp;		// Proportional constant
	double ki;		// Integral constant
	double kd;		// Derivative constant
	double dt;		// Standard change in time between updates (in seconds)
	double sr;		// Stable range
	double tv = 0;	// Target value
	double pe = 0;	// Previous error
	double pe2 = 0;	// Error before pe
	double in = 0;	// Integral value
	bool st = false;// Is the PID stable?

	// Constructors & Functions are visible to all
	public:

	// Default constructor (Don't use!)
	// PID() {
	// 	this->kp = 1;
	// 	this->ki = 0;
	// 	this->kd = 0;
	// 	this->dt = 1;
	// 	this->sr = 1;
	// }

	// Preferred constructor
	PID(double kp, double ki, double kd, double dt, double sr) {
		this->kp = kp;
		this->ki = ki;
		this->kd = kd;
		this->dt = dt;
		this->sr = sr;
	}

	// Set the target value of the PID
	void setTarget(double target) {
		this->tv = target;
	}

	double update(double sample) {
		// Calculate error, integral, and derivative
		double er = this->tv - sample;
		// Integral = (2*er + 8*pe - pe2)*dt/12
		this->in += ((2 * er) + (8 * this->pe) - this->pe2) * (this->dt / 12);
		// Derivative = (3*er - 4*pe + pe2)/(dt * 2)
		double de = ((3 * er) - (4 * this->pe) + this->pe2)/(this->dt * 2);

		// Shift time frame
		this->pe2 = this->pe;
		this->pe = er;

		// Determine if the PID is stable
		this->st = (abs(er * this->kp) + abs(this->in * this->ki) + abs(de * this->kd)) < this->sr;
		return (er * this->kp) + (this->in * this->ki) + (de * this->kd);
	}

	// Check if the PID has become stable;
	bool isStable() {
		return this->st;
	}

	double abs(double v) {
		if (v < 0) v *= -1;
		return v;
	}
};

extern PID drivePID;
extern PID turnPID;