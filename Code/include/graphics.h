#include "vex.h"
#include "variables.h"

using namespace vex;

using color = vex::color;

typedef struct {
  int id;
  int parentPageNumber;
  int buttonOnePageNumber;
  int buttonTwoPageNumber;
  int buttonThreePageNumber;
  int buttonFourPageNumber;
  int pageType;
  color pageColor;
  const char *name;
  const char *description;
} Page;

Page pages[] = {
//         ID PP B1 B2 B3 B4  TYPE    COLOR   NAME         DESC.
/*  0 */ {  0, 0, 1, 4, 7, 9, folder, NULL,   ""                                                         },
/* *1 */ {  1, 0, 2, 3, 0, 0, folder, red,    "Red"                                                      },
/*  2 */ {  2, 1, 0, 0, 0, 0, auton,  red,    "Red #1",       "Scores corner, middle goals | 3 points."  },
/*  3 */ {  3, 1, 0, 0, 0, 0, auton,  red,    "Red #2",       "Scores middle goal | 1 point."            },
/* *4 */ {  4, 0, 5, 6, 0, 0, folder, blue,   "Blue"                                                     },
/*  5 */ {  5, 4, 0, 0, 0, 0, auton,  blue,   "Blue #1",      "Scores corner, middle goals | 3 points."  },
/*  6 */ {  6, 4, 0, 0, 0, 0, auton,  blue,   "Blue #2",      "Scores middle goal | 1 point."            },
/* *7 */ {  7, 0, 8, 0, 0, 0, folder, yellow, "Skills"                                                   },
/*  8 */ {  8, 0, 0, 0, 0, 0, auton,  yellow, "Skills #1",    "Does skills stuff."                       },
/* *9 */ {  9, 0,10,11,12, 0, folder, green,  "Other"                                                    },
/* 10 */ { 10, 9, 0, 0, 0, 0, auton,  green,  "Testing #1",   "Uses new distance formula."               },
/* 11 */ { 11, 9, 0, 0, 0, 0, auton,  green,  "Testing #2",   "Runs Red #1 but with new formula."        },
/* 12 */ { 12, 9, 0, 0, 0, 0, auton,  green,  "Do Nothing",   "Literally does nothing."                  }
};

// Page pages[] = {
// //       ID PP B1 B2 B3 B4  TYPE     COLOR   NAME        DESC.
// /* 0 */ { 0, 0, 1, 2, 3, 4, auton,   NULL,   ""                                             },
// /* 1 */ { 1, 0, 0, 0, 0, 0, auton,   red,    "Red",     ""                                  }, 
// /* 4 */ { 2, 0, 0, 0, 0, 0, auton,   blue,   "Blue",    ""                                  }, 
// /* 7 */ { 3, 0, 0, 0, 0, 0, auton,   yellow, "Skills",  "__ points."                        }, 
// /* 8 */ { 4, 0, 5, 0, 0, 0, folder,  green,  "Other"                                        },
// /* 9 */ { 5, 4, 0, 0, 0, 0, auton,   green,  "Testing", "Strictly for testing auton stuff." }
// };