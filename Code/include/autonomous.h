#include "variables.h"
#include "PID.h"

#ifndef AUTONOMOUS_H
#define AUTONOMOUS_H

using namespace vex;
using string = std::string;

void step_end() { // calibrate, reset
  lft.resetRotation();
  rgt.resetRotation();
  roller.resetRotation();
  intake.resetRotation();

  auto_count++;
}

// determines how many degrees are rotated to complete the distance
double calc_move(int inch) { return round( inch / 0.01527 ); } 

// determines how many revolutions to achieve distance
double calc_revol(double inches) { return round( calc_move(inches) / 360 ); } 

void auto_step(int drive1, int drive2, double dist, double angle, int itk, int roll, double duration, int sensor) {  
  std::vector<string> order;

  int oppositeAlliance;
  (auto_select <= 3) ? oppositeAlliance = blueA : oppositeAlliance = redA;

  // double avgPos = (drive1 + drive2) / 2;

  // drivePID.setTarget(avgPos);
  // turnPID.setTarget(angle);

  intake.spin(directionType::undefined, itk, pct);
  roller.spin(directionType::undefined, -roll, pct);

  lft.spin(directionType::undefined, drive1, pct);
  rgt.spin(directionType::undefined, drive2, pct);

  // int aLeftMotorPower = ( drivePID.update(driveL) + turnPID.update(gyro_value) ) * 2;
  // int aRightMotorPower = ( turnPID.update(gyro_value) - drivePID.update(driveR) ) * 2;
  
  if (sensor == TIME) {
    task::sleep(duration);
    step_end();
  } /*else if (sensor == ENCO) {
    lft.rotateFor(directionType::undefined, calc_revol(dist), rev, false);
    rgt.rotateFor(directionType::undefined, calc_revol(dist), rev, false);
    if (lft.isDone() && rgt.isDone()) {
      step_end();
    }
  } else if (sensor == GYRO) {
    drv.turnFor(turnPID.update(gyro_value), deg, avgPos, rpm);
    if (drv.isDone()) {
      step_end();
    }
  } else if (sensor == LIFT) {
    while (Vision17.largestObject.exists) {
      switch (oppositeAlliance) {
        case blueA: // If we are Red, opposing alliance is Blue
          Vision17.takeSnapshot(BLUE_BALL);
          if (Vision17.objects[0].exists) {
            order.push_back("blue ");
          }
          break;
        case redA: // If we are Blue, opposing alliance is Red
          Vision17.takeSnapshot(RED_BALL);
          if (Vision17.objects[1].exists) {
            order.push_back("red ");
          }
          break;
      }
      if (order.size() > 3) order.front().erase();
    }
  } */else if (sensor == END) {
    step_end();
  }
}

#endif